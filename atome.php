<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="atome2.css">
    <link href="https://fonts.googleapis.com/css?family=Contrail+One&display=swap" rel="stylesheet"> 
    <title>Tableau périodique des éléments</title>
</head>

<body>

    <?php include 'pdo.php';

        try {

        $servername = '127.0.0.1';
        $username   = 'charly787';
        $password   = 'MaYa/2019';
        $db         = 'atome';

        $pdo = new PDO("mysql:host=$servername;dbname=$db;", $username, $password);
        $pdo->exec('SET CHARACTER SET utf8');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch(PDOException $e) {
            echo "Erreur :" . $e->getMessage();

        }

    ?>

    <table>

        <tr> <!-- 1e LIGNE-------------------------------------------------------------------------------------------------------------------------->
            <td class="gris"><!-- HYDROGÈNE 1 -->
                 <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=1");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td></td><td></td>

            <td></td>
            
            <td colspan="6">
                <h1>Tableau Périodique des Éléments</h1>
            </td>
            
            <td></td><td></td><td><td><td></td></td></td><td><td></td></td>

            <td class="bleu-fonce"><!-- HÉLIUM 2 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=2");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>
        </tr>


        <tr> <!-- 2 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td class="jaune"><!-- LITHIUM 3 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=3");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rouge"><!-- BÉRYLLIUM 4 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=4");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td></td>
            <td>
            <td class="padding" colspan="3" rowspan="2"><!-- COLONNE GAUGHE -->
                <p class="gris">Non-métaux</p>
                <p class="orange">Métaux alcalins</p>
                <p class="jaune">Métaux alcalino-terreux</p>
                <p class="violet">Métaux de transition</p>
                <p class="vert-fonce">Métaux pauvre</p>
            </td>

        

            <td class="padding" colspan="3" rowspan="2"><!-- COLONNE DROITE -->
                <p class="vert-clair">Métalloïdes</p>
                <p class="bleu-clair">Halogènes</p>
                <p class="bleu-fonce">Gaz nobles</p>
                <p class="rouge">Lanthanide</p>
                <p class="rose">Actinide</p>
            </td>
            </td>
            <td></td><td></td>

            <td class="vert-clair"><!-- BORE 5 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=5");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="gris"><!-- CARBONE 6 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=6");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="gris"><!-- AZOTE 7 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=7");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="gris"><!-- OXYGÈNE 8 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=8");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-clair"><!-- FLUORINE 9 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=9");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-fonce"><!-- NEON 10 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=10");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>
        </tr>
        

        <tr> <!-- 3 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td class="jaune"><!-- SODIUM 11 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=11");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rouge"><!-- MAGNÉSIUM 12 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=12");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td></td><td></td><td></td><td></td>

            <td class="vert-fonce"><!-- ALUMINIUM 13 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=13");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-clair"><!-- SILICIUM 14 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=14");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="gris"><!-- PHOSPHORE 15 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=15");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="gris"><!-- SULFURE 16 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=16");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-clair"><!-- CHLORINE 17 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=17");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-fonce"><!-- ARGON 18 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=18");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

        </tr>
        

        <tr> <!-- 4 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td class="jaune"><!-- POTASSIUM 19 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=19");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rouge"><!-- CALCIUM 20 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=20");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- SCANDIUM 21 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=21");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- TITANE 22 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=22");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- VANADIUM 23 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=23");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- CHROME 24 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=24");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- MANGANÈSE 25 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=25");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- FER 26 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=26");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- COBALT 27 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=27");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- NICKEL 28 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=28");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- CUIVRE 29 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=29");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- ZINC 30 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=30");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- GALILIUM 31 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=31");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-clair"><!-- GERMANIUM 32 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=32");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-clair"><!-- ARSENIC 33 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=33");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="gris"><!-- SÉLÉNIUM 34 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=34");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-clair"><!-- BROMINE 35 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=35");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-fonce"><!-- KRYPTON 36 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=36");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>
        </tr>
        

        <tr> <!-- 5 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td class="jaune"><!-- RUBIDIUM 37 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=37");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rouge"><!-- STRONTIUM 38 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=38");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- YTTRIUM 39 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=39");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- ZICHRONIUM 40 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=40");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- NIOBIUM 41 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=41");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- MOLYBDÈNE 42 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=42");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- TECHNÉTIUM 43 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=43");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- RUTHÉNIUM 44 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=44");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- RHODIUM 45 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=45");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- PALLADIUM 46 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=46");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- ARGENT 47 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=47");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- CADMIUM 48 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=48");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- INDIUM 49 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=49");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- ÉTAIN 50 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=50");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-clair"><!-- ANTIMOINE 51 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=51");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-clair"><!-- TELLURE 52 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=52");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-clair"><!-- IODINE 53 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=53");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-fonce"><!-- XENON 54 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=54");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>
        </tr>
        

        <tr> <!-- 6 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td class="jaune"><!-- CÉSIUM 55 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=55");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rouge"><!-- BARYUM 56 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=56");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose">57-71*</td>

            <td class="violet"><!-- HAFNIUM 72 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=72");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- TANTALE 73 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=73");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- TUNGSTÈNE 74 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=74");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- RHÉNIUM 75 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=75");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- OSMIUM 76 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=76");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- IRIDIUM 77 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=77");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- PLATINE 78 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=78");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- OR 79 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=79");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- MERCURE 80 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=80");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- THALLIUM 81 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=81");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- PLOMB 82 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=82");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- BISMUTH 83 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=83");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-clair"><!-- POLONIUM 84 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=84");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-clair"><!-- ASTATINE 85 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=85");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-fonce"><!-- RADON 86 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=86");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>
        </tr>


        <tr> <!-- 6 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td class="jaune marge"><!-- FRANCIUM 87 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=87");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rouge"><!-- RADIUM 88 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=88");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange">89-103**</td>

            <td class="violet"><!-- RUTHERFORDIUM 104 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=104");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- DUBNIUM 105 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=105");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- SEABORGIUM 106 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=106");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- BOHRIUM 107 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=107");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- HASSIUM 108 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=108");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- MEITNÉRIUM 109 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=109");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- DARMSTADIUM 110 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=110");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- ROENTGÉNIUM 111 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=111");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="violet"><!-- COPERNICIUM 112 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=112");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- UNUNTRIUM 113 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=113");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- FLÉROVIUM 114 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=114");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- UNUNPENTIUM 115 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=115");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="vert-fonce"><!-- LIVERMORIUM 116 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=116");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-clair"><!-- UNUNSEPTIUM 117 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=117");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="bleu-fonce"><!-- UNUNOCTIOM 118 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=118");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>
        </tr>


        <tr> <!-- 8 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td></td>
            <td id="pink-star">*</td>

            <td class="rose"><!-- LANTHANE 57 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=57");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- CÉRIUM 58 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=58");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- PRASÉODYME 59 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=59");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- NÉODYME 60 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=60");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- PROMÉTHIUM 61 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=61");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- SAMARIUM 62 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=62");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- EUROPIUM 63 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=63");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- GADOLINIUM 64 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=64");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- TERBIUM 65 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=65");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- DYSPROSIUM 66 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=66");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- HOLMIUM 67 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=67");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- ERBIUM 68 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=68");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- THULIUM 69 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=69");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- YTTERBIUM 70 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=70");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="rose"><!-- LUCÉTIUM 71 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=71");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td></td>
        </tr>


        <tr> <!-- 9 LIGNE -------------------------------------------------------------------------------------------------------------------------->
            <td></td>
            <td id="orange-star">**</td>

            <td class="orange"><!-- ACTINIUM 89 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=89");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- THORIUM 90 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=90");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- PROTACTINIUM 91 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=91");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- URANIUM 92 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=92");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- NEPTUNIUM 93 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=93");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- PLUTONIUM 94 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=94");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- AMÉRICIUM 95 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=95");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- CURIUM 96 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=96");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- BERKÉLIUM 97 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=97");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- CALIFORNIUM 98 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=98");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- EINSTEINIUM 99 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=99");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- FERMIUM 100 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=100");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- MANDÉLÉVIUM 101 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=101");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- NOBÉLIUM 102 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=102");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td class="orange"><!-- LAWRENCIUM 103 -->
                <?php
                    try {
                        $request = $pdo->prepare("SELECT * FROM atome WHERE id=103");
                        $request->execute();
                        $request = $request->fetchAll();
                    }
                    catch (PDOException $e) {
                        echo 'erreur' . $e->getMessage();
                    }
                    foreach ($request as $atome) {
                        echo $atome['id'].'<br>'.$atome['symbole'].'<br>'.$atome['nom'].'<br>'.$atome['masse_atomique'];
                    }
                ?>
            </td>

            <td></td>
        </tr>

    </table>
